import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { InterestRate } from './InterestRate.model';

@Injectable({
  providedIn: 'root'
})
export class InterestClientServiceService {
  interestApiUrl = 'http://localhost:9091/calculate/calcInterest'

  constructor(private _http: HttpClient) { }


  getInterestRate(agreementType:string,startDate:string,endDate:string,amount:string) {
    const queryParams = new HttpParams()
    .set('agreementType',agreementType)
    .set('amount',amount)
    .set('startDate',startDate)
    .set('endDate',endDate);
    return this._http.get<InterestRate>(this.interestApiUrl,{params:queryParams});
  }
}
