import { Component, OnInit } from '@angular/core';
import { InterestClientServiceService } from '../interest-client-service.service';
import { InterestRate } from '../InterestRate.model';


@Component({
  selector: 'app-rate-client',
  templateUrl: './rate-client.component.html',
  styleUrls: ['./rate-client.component.css']
})
export class RateClientComponent implements OnInit {

  showResult: boolean = false;
  interestRate$: InterestRate

  amount: string ='0.0'
  startDate: string ='2019-01-01'
  endDate: string ='2020-01-01'
  agreementType: string 

  constructor(private interestService: InterestClientServiceService) { }

  ngOnInit() {
  }


  calculateROI(): void {
    if(this.agreementType==undefined)
    this.agreementType='1';
    this.interestService.getInterestRate(this.agreementType, this.startDate, this.endDate, this.amount)
      .subscribe(data => { this.interestRate$ = data
      console.log("value from server"+this.interestRate$);
          if(this.interestRate$.value != "0.0" )
          this.showResult=true
          else
          this.showResult=false
      })


  }
}
