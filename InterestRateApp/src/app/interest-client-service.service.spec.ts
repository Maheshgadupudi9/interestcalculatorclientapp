import { TestBed } from '@angular/core/testing';

import { InterestClientServiceService } from './interest-client-service.service';

describe('InterestClientServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterestClientServiceService = TestBed.get(InterestClientServiceService);
    expect(service).toBeTruthy();
  });
});
